HEROKU_KEY='ee0ea9a3-1278-498e-8dda-956363be7d03'
HEROKU_USERNAME=khalilhamze.bas@gmail.com
FRONTEND_APP_NAME='compugear-r01'
BACKEND_APP_NAME='flask-mongo-r01'

# Login to heroku
# Your heroku api key can be found in your account settings
docker login -u $HEROKU_USERNAME -p $HEROKU_KEY registry.heroku.com

# Build the applications and tag them accordingly
docker build --file=frontend/Dockerfile --rm=true -t registry.heroku.com/$FRONTEND_APP_NAME/web ./frontend/
# docker build --file=backend/Dockerfile --rm=true -t registry.heroku.com/$BACKEND_APP_NAME/web ./backend/

# Push the images to heroku
docker push registry.heroku.com/$FRONTEND_APP_NAME/web
# docker push registry.heroku.com/$BACKEND_APP_NAME/web

# Release/Run the apps
heroku container:release web -a $FRONTEND_APP_NAME
# heroku container:release web -a $BACKEND_APP_NAME